// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

export class OptionValue {
  id: string;
  name: string;
  presentation: string;
  option_type_name: string;
  option_type_id: number;
  option_type_presentation: string;
}
