// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

export class Job {
  id: string;
  jobTitle: string;
  description: string;
  createdAt: string;
  updatedAt: string;
}
