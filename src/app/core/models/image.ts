// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

export class Image {
  id: number;
  position: number;
  attachment_content_type: string;
  attachment_file_name: string;
  type: string;
  attachment_updated_at: string;
  attachment_width: number;
  attachment_height: number;
  alt: string;
  viewable_type: string;
  viewable_id: number;
  mini_url: string;
  small_url: string;
  product_url: string;
  large_url: string;
}
