// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { Response, RequestOptions, RequestOptionsArgs, Headers } from '@angular/http';
import { HttpService } from './http';
import { AppState } from '../../interfaces';
import { Store } from '@ngrx/store';
import { AuthActions } from '../../auth/actions/auth.actions';

@Injectable()
export class AuthService {

  /**
   * Creates an instance of AuthService.
   * @param {HttpService} http
   * @param {AuthActions} actions
   * @param {Store<AppState>} store
   *
   * @memberof AuthService
   */
  constructor(
    private http: HttpService,
    private actions: AuthActions,
    private store: Store<AppState>
  ) {

  }

  /**
   *
   *
   * @param {any} data
   * @returns {Observable<any>}
   *
   * @memberof AuthService
   */
  login(data): Observable<any> {
    let requestOption: RequestOptionsArgs = new RequestOptions();
    requestOption.headers = new Headers({
      'Content-Type': 'application/json',
      'X-Requested-With': 'XMLHttpRequest',
      'iss': data.iss
    });
    var newData = {username: data.username, password: data.password}
    return this.http.post(
      'auth/login',
      newData,
      requestOption
    ).map((res: Response) => {
      data = res.json();
      console.log(res)
      if (res.ok) {
        // Setting token after login
        this.setTokenInLocalStorage(data);
        this.store.dispatch(this.actions.loginSuccess());
      } else {
        this.http.loading.next({
          loading: false,
          hasError: true,
          hasMsg: res.json().message
        });
      }
      return data;
    });

  }


  /**
   *
   *
   * @param {any} data
   * @returns {Observable<any>}
   *
   * @memberof AuthService
   */
  register(data): Observable<any> {
    return this.http.post(
      'auth/user',
      { user : data }
    ).map((res: Response) => {
      data = res.json();
      if (res.ok) {
        // Setting token after login
        this.setTokenInLocalStorage(res.json());
        this.store.dispatch(this.actions.loginSuccess());
      } else {
        this.http.loading.next({
          loading: false,
          hasError: true,
          hasMsg: 'Please enter valid Credentials'
        });
      }
      return res.json();
    });
  }

  /**
   *
   *
   * @returns {Observable<any>}
   *
   * @memberof AuthService
   */
  authorized(): Observable<any> {
    return this.http
      .get('me')
      .map((res: Response) => {
        if(res.ok){
          const jsonData = JSON.stringify(res.json());
          localStorage.setItem('me', jsonData);
        }
        return res
      });
  }

  getCodeReset(account:string): Observable<any> {
    return this.http.get(`pub/reset/code?account=${account}`)
    .map((res: Response) => {
      if(res.json().status != 500){
        this.store.dispatch(this.actions.resetSuccess());
        localStorage.setItem('account', account);
      }else{
        this.http.loading.next({
          loading: false,
          hasError: true,
          hasMsg: 'Please input valid username or email'
        });
      }
      return res;
    })
  }

  verifyPassword(code:string){
    let account = localStorage.getItem('account');
    return this.http.get(`pub/reset/verify?code=${code}&account=${account}`)
    .map((res: Response) =>{
      if(res.ok){
        this.store.dispatch(this.actions.resetSuccess());
        localStorage.setItem('code', code);
      }else{
        this.http.loading.next({
          loading: false,
          hasError: true,
          hasMsg: res.json().message
        })
      }
      return res;
    })
  }

  resetPassword(object): Observable<any> {
    let requestOption: RequestOptionsArgs = new RequestOptions();
    requestOption.headers = new Headers({
      'Content-Type': 'application/json',
      'X-Requested-With': 'XMLHttpRequest'
    });
    let account = localStorage.getItem('account');
    let code = localStorage.getItem('code');
    Object.assign(object,{code: code, account: account});
    return this.http.put(`pub/reset/password`, object, requestOption)
    .map((res: Response) => {
      if(res.ok){
        this.store.dispatch(this.actions.resetSuccess());
        localStorage.removeItem('account');
        localStorage.removeItem('code');
      }else{
        this.http.loading.next({
          loading: false,
          hasError: true,
          hasMsg: res.json().message
        });
      }
      return res;
    })
  }

  /**
   *
   *
   * @returns
   *
   * @memberof AuthService
   */
  logout() {
    return this.http.get('auth/user')
      .map((res: Response) => {
        // Setting token after login
        localStorage.removeItem('u_t');
        this.store.dispatch(this.actions.logoutSuccess());
        return res.json();
      });
  }

  /**
   *
   *
   * @private
   * @param {any} user_data
   *
   * @memberof AuthService
   */
  private setTokenInLocalStorage(user_data): void {
    const jsonData = JSON.stringify(user_data);
    localStorage.setItem('u_t', jsonData);
  }

  public getGroups(): Observable<any> {
    let requestOption: RequestOptionsArgs = new RequestOptions();
    requestOption.headers = new Headers({
      'Content-Type': 'application/json'
    });

    return this.http.get(`pub/groups`, requestOption).map((res)=>{
      if(!res.ok){
        this.http.loading.next({
          loading: false,
          hasError: true,
          hasMsg: res.json().message
        })
      }
      return res;
    })

  }

}
