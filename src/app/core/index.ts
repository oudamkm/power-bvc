// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { NgModule } from '@angular/core';
import { HttpModule, XHRBackend, RequestOptions, Http } from '@angular/http';
// Components

// All Services
import { AuthService } from './services/auth.service';
import { HttpService } from './services/http';
import { UserService } from '../user/services/user.service';
import { SettingService } from '../settings/services/settings.service';
import { LangService } from './services/lang.service';

import { EffectsModule } from '@ngrx/effects';

// All actions
import { AuthActions } from '../auth/actions/auth.actions';
import { UserActions } from '../user/actions/user.actions';
import { SettingActions } from '../settings/actions/settings.actions';

import { AuthenticationEffects } from '../auth/effects/auth.effects';
import { UserEffects } from '../user/effects/user.effects';
import { SettingEffects } from '../settings/effects/settings.effects';


import { CanActivateViaAuthGuard } from './guards/auth.guard';


export function httpInterceptor(
  backend: XHRBackend,
  defaultOptions: RequestOptions,
) {
  return new HttpService(backend, defaultOptions);
}

@NgModule({
  declarations: [
  ],
  exports: [
  ],
  imports: [
    EffectsModule.run(AuthenticationEffects),
    EffectsModule.run(UserEffects)
  ],
  providers: [
    AuthService,
    {
      provide: HttpService,
      useFactory: httpInterceptor,
      deps: [ XHRBackend, RequestOptions]
    },
    AuthActions,
    UserActions,
    UserService,
    SettingActions,
    SettingService,
    CanActivateViaAuthGuard,
    LangService
  ]
})
export class CoreModule {}
