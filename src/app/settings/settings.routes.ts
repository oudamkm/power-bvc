// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { SettingComponent } from './settings.component';
import { JobComponent } from './components/job/job.component';
import { MeterComponent } from './components/meter/meter.component';

import { CanActivateViaAuthGuard } from '../core/guards/auth.guard';

export const SettingRoutes = [
  {
    path: '',
    component: SettingComponent,
    children: [
      { path: 'job', component: JobComponent },
      { path: 'setup', children:[
        {path: 'meter', component: MeterComponent}
      ]}
    ]
  },
];
