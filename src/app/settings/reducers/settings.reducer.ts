// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { SettingState, SettingStateRecord } from './settings.state';
import { Action, ActionReducer } from '@ngrx/store';
import { SettingActions } from '../actions/settings.actions';
import { Job } from '../../core/models/job';
export const initialState: SettingState = new SettingStateRecord() as SettingState;

export const settingReducer: ActionReducer<SettingState> =
    (state: SettingState = initialState, { type, payload }: Action): SettingState => {
        switch (type) {
            case SettingActions.FETCH_ALL_JOBS_SUCCESS:
                return state.merge({
                    jobs: payload.jobs
                }) as SettingState;
            case SettingActions.DELETE_JOB_SUCCESS:
                return state.merge({
                    message: payload.message
                }) as SettingState;

            case SettingActions.CREATE_JOB_SUCCESS:
                return state.merge({
                    message: payload.message
                }) as SettingState

            default:
                return state;

        }
    }