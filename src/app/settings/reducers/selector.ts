// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { AppState } from './../../interfaces';
import { createSelector } from 'reselect';
import { SettingState } from './settings.state';
import { List } from 'immutable';

// Base state function
/**
 * @param {AppState} state
 * @returns {UserState}
 */
function getSettingState(state: AppState): SettingState {
    return state.settings;
}

// Individual selector

export function fetchAllJobs(state : SettingState){
  const jobs = state.jobs;
  return jobs.toJS();
}
export function deleteJobById(state: SettingState){
  const message = state.message;
  return message.toJS();
}
export function createJobs(state: SettingState){
  const message = state.message;
  return message;
}

// public api
export const getJobs = createSelector(getSettingState, fetchAllJobs);
export const deleteJob = createSelector(getSettingState, deleteJobById);
export const createJob = createSelector(getSettingState, createJobs);