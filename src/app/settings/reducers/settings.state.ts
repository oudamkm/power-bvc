// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { Map, Record, List,fromJS } from 'immutable';
import { Job } from '../../core/models/job';

/**
 * @export
 * @interface SettingState
 * @extends {Map<string, any>}
 */
export interface SettingState extends Map<string, any> {
  jobs: Map<string, any>;
  message: Map<string, any>;
  error: Map<string, any>;
}

export const SettingStateRecord = Record({
  jobs: Map({}),
  message: Map({}),
  error: Map({})
});
