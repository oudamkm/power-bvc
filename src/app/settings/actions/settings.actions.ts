// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { Action } from '@ngrx/store';
import { Job } from '../../core/models/job';
import { Meter } from '../../core/models/meter';

export class SettingActions {
  static FETCH_ALL_JOBS = 'FETCH_ALL_JOBS';
  static FETCH_ALL_JOBS_SUCCESS = 'FETCH_ALL_JOBS_SUCCESS';
  static CREATE_JOB = 'CREATE_JOB';
  static CREATE_JOB_SUCCESS = 'CREATE_JOB_SUCCESS';
  static CREATE_JOB_FAILURE = 'CREATE_JOB_FAILURE';
  static DELETE_JOB = "DELETE_JOB";
  static DELETE_JOB_SUCCESS = "DELETE_JOB_SUCCESS";
  static DELETE_JOB_FAILURE = "DELETE_JOB_FAILURE";

  static FETCH_ALL_METER = 'FETCH_ALL_METE';
  static CREATE_METER = 'CREATE_METER';
  static UPDATE_METER = 'UPDATE_METER';

  fetchJobs(pageSize, nowPage, orderBy) {
    return {
       type: SettingActions.FETCH_ALL_JOBS,
       payload: {
         pageSize,
         nowPage,
         orderBy
       }
     };
  }

  fetchJobsSuccess(jobs: any) {
    return {
      type: SettingActions.FETCH_ALL_JOBS_SUCCESS,
      payload: jobs
    };
  }

  createJob(job:Job){
    return { type: SettingActions.CREATE_JOB, payload:job }
  }

  createJobSuccess(data:any){
    return { type: SettingActions.CREATE_JOB_SUCCESS, payload: data}
  }
  
  createJobFailure(data:any){
    return { type: SettingActions.CREATE_JOB_FAILURE, payload: data}
  }

  deleteJob(id:number){
    return {
      type: SettingActions.DELETE_JOB,
      payload:id
    }
  }

  deleteJobSuccess(data:any){
    return {
      type: SettingActions.DELETE_JOB_SUCCESS,
      payload:data
    }
  }

  deleteJobFailure(error: any){
    return {
      type: SettingActions.DELETE_JOB_FAILURE,
      payload: error
    }
  }

  // Get, Create and Update Meter  
  fetchMeters( pageSize, nowPage, orderBy){
    return { type: SettingActions.FETCH_ALL_METER, payload: { pageSize, nowPage, orderBy}}
  }

  createMeter( meter:Meter ){
    return { type: SettingActions.CREATE_METER, payload: meter }
  }

  updateMeter( meter: Meter){
    return { type: SettingActions.UPDATE_METER, payload: meter}
  }
}