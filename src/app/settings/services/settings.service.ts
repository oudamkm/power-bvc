// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { Injectable } from '@angular/core';
import { HttpService } from '../../core/services/http';
import { SettingActions } from '../actions/settings.actions';
import { Store } from '@ngrx/store';
import { AppState } from '../../interfaces';
import { Response } from '@angular/http';
import { Job } from '../../core/models/job';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class SettingService {

  constructor(
    private http: HttpService,
    private actions: SettingActions,
    private store: Store<AppState>
  ) { }

  /**
   * @returns {Observable<Job>}
   * @memberof SettingService
   */
  getJobs(pageSize, nowPage, orderBy): Observable<any> {
    return this.http.get(`jobtitles?pageSize=${pageSize}&nowPage=${nowPage}&orderBy=${orderBy}`)
      .map(res => res);
  }

  /**
   * @returns {Observable<Job>}
   * @memberof SettingService
   */
  createJob(job:Job): Observable<any>{
    return this.http.post(`jobtitles`, job).map(res =>{ 
      if(!res.ok){
        this.http.loading.next({
          loading: false,
          hasError: true,
          hasMsg: `internal_server_error` 
        });
      }
       return res; 
    })
  }

  deleteJob(id:number): Observable<any>{
    return this.http.delete(`jobtitles/${id}`)
    .map(res => res)
    .catch((res)=>{
      return Observable.throw(res.json());
    });
  }

}