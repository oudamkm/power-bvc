// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { Component, OnInit, OnChanges, Input, Inject } from '@angular/core';
import { LangService } from '../core/services';
import { TranslatorContainer } from '../shared/modules/translator'; 
import { SettingActions } from './actions/settings.actions';
import { Store } from '@ngrx/store';
import { AppState } from '../interfaces';

@Component({
    selector: 'app-home',
    template: `
    <div class="page-content">
    <div class="container-fluid">
        <div class="row">
            <router-outlet></router-outlet>
        </div>
    </div>
    </div>
  `,
    styleUrls: ['./settings.component.less']
})
export class SettingComponent implements OnInit {
    constructor(private langService: LangService,
    public translatorContainer:TranslatorContainer,
    private settingActions:SettingActions,
    private store:Store<AppState>) {
    }

    ngOnInit() {
        this.langService.getData().subscribe((res)=>{
            this.translatorContainer.language = String(res);
        })
    }

}
