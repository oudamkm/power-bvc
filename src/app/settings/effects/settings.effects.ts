// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { Injectable } from '@angular/core';
import { Actions, Effect, toPayload } from '@ngrx/effects';
import { SettingService } from '../services/settings.service'; 
import { SettingActions } from '../actions/settings.actions';
import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';

@Injectable()
export class SettingEffects {
  constructor(private actions$: Actions,
              private settingService: SettingService,
              private settingActions: SettingActions) { }

  // tslint:disable-next-line:member-ordering
  @Effect()
    GetAllJobs$: Observable<Action> = this.actions$
    .ofType(SettingActions.FETCH_ALL_JOBS)
    .map(toPayload)
    .switchMap((payload) => this.settingService.getJobs(
        payload.pageSize, payload.nowPage, payload.orderBy))
    .filter((data)=> data.ok && data.json() )
    .map((data: any) => {
        return  this.settingActions.fetchJobsSuccess({jobs: data.json().items });
    });
     
   @Effect()
        DeleteJob$: Observable<Action> = this.actions$
        .ofType(SettingActions.DELETE_JOB)
        .map(toPayload)
        .switchMap((payload) => {
           return this.settingService.deleteJob(payload)
        })
        .filter((data) => data.ok)
        .map((data: any) => {
            return this.settingActions.deleteJobSuccess({message: data.json()})
        })

    @Effect()
        CreateJob$: Observable<Action> = this.actions$
        .ofType(SettingActions.CREATE_JOB)
        .map(toPayload)
        .switchMap((payload)=> this.settingService.createJob(payload))
        .filter((data)=> data)
        .map((data:any)=>{
            return this.settingActions.createJobSuccess({message: data});
        })

}