// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { SharedModule } from './../shared/index';

// Components
import { SettingComponent } from './settings.component';
import { JobComponent } from './components/job/job.component';
import { MeterComponent } from './components/meter/meter.component';
// Service
import { SettingService } from './services/settings.service';
import { LayoutModule } from '../layout/index';
// Routes
import { SettingRoutes as routes } from './settings.routes';
// Action
import { SettingActions } from './actions/settings.actions';

// Effect
import { SettingEffects } from './effects/settings.effects';
@NgModule({
    declarations: [
        // components
        SettingComponent,
        JobComponent,
        MeterComponent
    ],
    exports: [
        JobComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        EffectsModule.run(SettingEffects),
        SharedModule,
        LayoutModule
    ],
    providers: [
    
    ]
})
export class SettingModule { }
