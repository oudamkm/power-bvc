// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { Component, OnInit, OnDestroy, ViewChild, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Job } from '../../../core/models/job';
import { DataTable, DataTableTranslations, DataTableResource } from '../../../shared/modules';
import { SettingService } from '../../services/settings.service';
import { getJobs } from '../../reducers/selector';
import { deleteJob, createJob } from '../../reducers/selector';
// Action
import { SettingActions } from '../../actions/settings.actions';

import { Store } from '@ngrx/store';
import { AppState } from '../../../interfaces';

import { provideTranslator, TranslatorContainer } from '../../../shared/modules';
import { ModalComponent, normFieldConfig, ToasterModule, ToasterService, ToasterConfig, Toast } from '../../../shared/modules';
import { LayoutModule } from '../../../layout';
@Component({
    selector: 'app-job',
    templateUrl: './job.component.html',
    providers: [provideTranslator('job')]
})
export class JobComponent implements OnInit {

    jobCount = 0;
    jobResource = null;
    private jobs$: Observable<any>;
    private jobs$$: Subscription;
    private createJob$: Observable<any>;
    public jobs: Job[] = [];
    public jobTitle: FormGroup
    public paging = { pageSize: 8, nowPage: 1, orderBy: 'id' };
    JobTitleModel: any = {};
    public jobTitleFields: Array<normFieldConfig> = [];
    options;
    @ViewChild('modal')
    private modal: ModalComponent;
    @ViewChild(DataTable) jobsTable;
    public lang: string;
    private me = this;
    constructor(private settingService: SettingService,
        private store: Store<AppState>,
        private settingActions: SettingActions, private fb: FormBuilder,
        private translatorContainer: TranslatorContainer,
        private toasterService: ToasterService) {
        console.log("First Init")
        this.jobs$ = this.store.select(getJobs);
        this.createJob$ = this.store.select(createJob);


        let jobTitleFields: Array<normFieldConfig> = [{
            className: '',
            fieldGroup: [{
                className: '',
                key: 'jobTitle',
                type: 'input',
                templateOptions: {
                    type: 'text',
                    label: 'Job Title',
                    placeholder: 'Job Title',
                    required: true,
                    minLength: 5
                },
                modelOptions: {
                    debounce: {
                        default: 2000,
                        blur: 0,
                    },
                    updateOn: 'default blur',
                },
                focus: true,
                validators: {
                    validation: Validators.compose([Validators.required])
                },
                validation: {
                    messages: {
                        required: 'Please!! Enter the Job Title',
                    },
                }
            }, {
                className: '',
                key: 'description',
                type: 'input',
                templateOptions: {
                    type: 'text',
                    label: 'Description',
                    placeholder: 'Description',
                    pattern: '',
                    required: true,
                },
                validators: {
                    validation: Validators.compose([Validators.required])
                },
                validation: {
                    messages: {
                        required: 'Please!! Enter the Description',
                    },
                },
                //  expressionProperties: {
                //      'validation.show': 'model.checked === true ? true: null',
                //     'templateOptions.disabled': '!formState.readOnly',
                //  },
                // expressionProperties: {
                //     'templateOptions.disabled': '!model.jobTitle',
                // },
            }]
        }];
        setTimeout(() => this.jobTitleFields = jobTitleFields);
        this.options = {
            formState: {
                readOnly: true,
                submited: false
            },
        };
        this.JobTitleModel = {
           jobTitle: '',
           description: ''
        }

    }

    // JobTitle = {
    //     jobTitle: '',
    //     description: ''
    // }

    popToast(type: string, title: string, body: string) {
        var toast: Toast = {
            type: type,
            title: title,
            body: body
        };

        this.toasterService.pop(toast);
    }

    requestJobs(paging) {
        this.store.dispatch(this.settingActions.fetchJobs(paging.pageSize,
            paging.nowPage,
            paging.orderBy));
    }

    ngOnInit() {
        this.requestJobs(this.paging);
        this.jobTitle = this.fb.group({});
        this.jobs$$ = this.jobs$.filter((data) => data.list != null).subscribe((res) => {
            this.jobs = res.list;
            this.jobCount = res.totalCount
            this.jobResource = new DataTableResource(this.jobs);
        })
        this.createJob$.subscribe((res) => {
            if (res.ok) {
                this.modal.close();
                this.requestJobs(this.paging);
            }
        })
    }

    ngOnDestroy() {
        this.jobs$$.unsubscribe();
    }

    reloadJobs(params) {
        this.requestJobs(this.paging);
    }

    cellColor(car) {
        return 'rgb(255, 255,' + (155 + Math.floor(100 - ((car.rating - 8.7) / 1.3) * 100)) + ')';
    };

    // special params:

    translations = <DataTableTranslations>{
        indexColumn: 'index_column',
        expandColumn: 'Expand column',
        selectColumn: 'Select column',
        paginationLimit: 'Max results',
        paginationRange: 'Result range'
    };

    onAddJobTitle(e) {
        let obj = Object.assign({}, e);
        this.store.dispatch(this.settingActions.createJob(obj));
    }

    eventPageForward(e) {
        Object.assign(this.paging, e);
        this.requestJobs(this.paging);
    }

    eventPageBackward(e) {
        Object.assign(this.paging, e);
        this.requestJobs(this.paging);
    }

    eventLimitPage(e) {
        Object.assign(this.paging, e);
        this.requestJobs(this.paging);
    }

    eventPageLast(e) {
        Object.assign(this.paging, e);
        this.requestJobs(this.paging);
    }

    eventPageFirst(e) {
        Object.assign(this.paging, e);
        this.requestJobs(this.paging);
    }

    eventDelete(e) {
        this.store.dispatch(this.settingActions.deleteJob(e.rows[0].item.id));
    }
    eventAdd() {
        this.modal.open();
    }
}