// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { Component, OnInit, OnDestroy, ViewChild, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Job } from '../../../core/models/job';
import { DataTable, DataTableTranslations, DataTableResource } from '../../../shared/modules';
import { SettingService } from '../../services/settings.service';
import { getJobs } from '../../reducers/selector';
import { deleteJob, createJob } from '../../reducers/selector';
// Action
import { SettingActions } from '../../actions/settings.actions';

import { Store } from '@ngrx/store';
import { AppState } from '../../../interfaces';

import { provideTranslator, TranslatorContainer } from '../../../shared/modules';
import { ModalComponent, normFieldConfig, ToasterModule, ToasterService, ToasterConfig, Toast } from '../../../shared/modules';
import { LayoutModule } from '../../../layout';
@Component({
    selector: 'setting-meter',
    templateUrl: './meter.component.html',
    providers: [provideTranslator('job')]
})
export class MeterComponent implements OnInit {

    jobCount = 0;
    jobResource = null;
    private jobs$: Observable<any>;
    private jobs$$: Subscription;
    public jobs: Job[] = [];
    public jobTitle: FormGroup
    public paging = { pageSize: 8, nowPage: 1, orderBy: 'id' }
    @ViewChild('modal')
    private modal: ModalComponent;
    @ViewChild(DataTable) jobsTable;
    public lang: string;
    constructor(private settingService: SettingService,
        private store: Store<AppState>,
        private settingActions: SettingActions, private fb: FormBuilder,
        private translatorContainer: TranslatorContainer,
        private toasterService: ToasterService) {

        console.log("First Init")
        this.jobs$ = this.store.select(getJobs);

    }


    popToast(type: string, title: string, body: string) {
        var toast: Toast = {
            type: type,
            title: title,
            body: body
        };

        this.toasterService.pop(toast);
    }

    requestJobs(paging) {
        this.store.dispatch(this.settingActions.fetchJobs(paging.pageSize,
            paging.nowPage,
            paging.orderBy));
    }

    ngOnInit() {
        this.requestJobs(this.paging);
        this.jobTitle = this.fb.group({});
        this.jobs$$ = this.jobs$.filter((data) => data.list != null).subscribe((res) => {
            this.jobs = res.list;
            this.jobCount = res.totalCount
            this.jobResource = new DataTableResource(this.jobs);
        })
    }

    ngOnDestroy() {
        this.jobs$$.unsubscribe();
    }
    reloadJobs(params) {
        this.requestJobs(this.paging);
        // this.jobResource.query(params).then(jobs => this.jobs = jobs);
    }

    cellColor(car) {
        return 'rgb(255, 255,' + (155 + Math.floor(100 - ((car.rating - 8.7) / 1.3) * 100)) + ')';
    };

   
    // special params:

    translations = <DataTableTranslations>{
        indexColumn: 'index_column',
        expandColumn: 'Expand column',
        selectColumn: 'Select column',
        paginationLimit: 'Max results',
        paginationRange: 'Result range'
    };

    jobTitleFields: normFieldConfig = [{
        className: '',
        fieldGroup: [{
            className: '',
            key: 'jobTitle',
            type: 'input',
            templateOptions: {
                type: 'text',
                label: '',
                placeholder: 'Job Title'
            },
            validators: {
                validation: Validators.compose([Validators.required])
            }
        }, {
            className: '',
            key: 'description',
            type: 'input',
            templateOptions: {
                type: 'text',
                label: '',
                placeholder: 'Description',
                pattern: ''
            },
            validators: {
                validation: Validators.compose([Validators.required])
            }
        }]
    }];
    JobTitle = {
        jobTitle: '',
        description: ''
    }

    onAddJobTitle(e) {
        this.store.dispatch(this.settingActions.createJob(e))
        this.store.select(createJob).subscribe((res)=>{
           if(res){
                this.requestJobs(this.paging);
           }
        })
        
    }

    eventPageForward(e) {
        Object.assign(this.paging, e);
        this.requestJobs(this.paging);
        console.log(e)
    }

    eventPageBackward(e) {
        Object.assign(this.paging, e);
        this.requestJobs(this.paging);
        console.log(e)
    }

    eventLimitPage(e) {
        Object.assign(this.paging, e);
        this.requestJobs(this.paging);
        console.log(e)
    }

    eventPageLast(e) {
        Object.assign(this.paging, e);
        this.requestJobs(this.paging);
    }

    eventPageFirst(e) {
        Object.assign(this.paging, e);
        this.requestJobs(this.paging);
    }

    eventDelete(e) {
        this.store.dispatch(this.settingActions.deleteJob(e.rows[0].item.id));
        this.store.select(deleteJob).subscribe((res: any) => {
            console.log(res)
           if(res.messageId){
               this.popToast('info', '', res.messageId);
           }
        }, (error) => {
            if(error.messageId){
               this.popToast('error', '', error.messageId);
           }
            console.log(error)
        })
    }
    eventAdd() {
        this.modal.open();
    }
}