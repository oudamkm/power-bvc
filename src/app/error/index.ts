// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { SharedModule } from './../shared/index';

// Components
import { ErrorComponent } from './error.component';
// Breadcrumb components


// Content components

// Sidebar components


import { LayoutModule } from '../layout/index';
// Routes
import { ErrorRoutes as routes } from './error.routes';

@NgModule({
    declarations: [
        // components
        ErrorComponent
    ],
    exports: [
    ],
    imports: [
        RouterModule.forChild(routes),
        SharedModule,
        LayoutModule
    ],
    providers: [

    ]
})
export class ErrorModule { }
