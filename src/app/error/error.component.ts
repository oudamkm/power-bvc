// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { Component, OnInit, OnChanges } from '@angular/core';

@Component({
    selector: 'app-home',
    template: `
    <div class="container-fluid">
      <div class="row">
      <div class="page-error-box">
        <div class="error-code">404</div>
        <div class="error-title">Page not found</div>
          <a routerLink="/" class="btn btn-rounded">Main page</a>
        </div>

        <router-outlet></router-outlet>
      </div>
    </div>
  `,
    styleUrls: ['./error.component.less']
})
export class ErrorComponent implements OnInit {

    constructor() {
    }

    ngOnInit() {

    }

}
