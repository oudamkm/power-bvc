// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { ErrorComponent } from './error.component';
import { CanActivateViaAuthGuard } from '../core/guards/auth.guard';

export const ErrorRoutes = [
  { path: '', component: ErrorComponent },
];
