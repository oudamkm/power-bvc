// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { AuthState } from './auth/reducers/auth.state';
import { UserState } from './user/reducers/user.state';
import { SearchState } from './home/reducers/search.state';
import { SettingState } from './settings/reducers/settings.state';
// This should hold the AppState interface
// Ideally importing all the substate for the application

/**
 *
 *
 * @export
 * @interface AppState
 */
export interface AppState {
  auth: AuthState;
  users: UserState;
  search: SearchState;
  settings: SettingState;
}
