// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { getAuthStatus } from './auth/reducers/selectors';
import { AppState } from './interfaces';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs/Subscription';
import { Component, OnInit, OnDestroy, Inject, Output} from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Translator, TranslatorContainer } from './shared/modules/translator';
import { AuthActions } from './auth/actions/auth.actions';
import { Observable } from 'rxjs/Observable';
import { LangService } from './core/services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit, OnDestroy {
  currentUrl: string;
  currentStep: string;
  public title = 'Loading...';
  isAuthenticated: Observable<boolean>;
  public translations: object = {};
  constructor(
    private router: Router,
    private store: Store<AppState>,
    private authActions: AuthActions,
    @Inject(Translator) private translator: Translator,
    @Inject(TranslatorContainer) public translatorContainer: TranslatorContainer,
    @Inject(LangService) private langService: LangService
    ) {
      this.translatorContainer = translatorContainer;
      this.store.dispatch(this.authActions.authorize());
    router
      .events
      .filter(e => e instanceof NavigationEnd)
      .subscribe((e: NavigationEnd) => {
        this.currentUrl = e.url;
        window.scrollTo(0, 0);
      });

      const promises = [];

    Promise.all(promises).then(() => {
      console.log(this.translations);
    });
  }

  ngOnInit() {
    this.store.dispatch(this.authActions.authorize());
    this.isAuthenticated = this.store.select(getAuthStatus);
  }

  ngOnDestroy() {
  }

  onChangeLangauge(e){
    this.langService.updateData(e);
    this.translatorContainer.language = e;
  }

}
