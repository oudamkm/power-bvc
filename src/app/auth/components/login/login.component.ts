// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { Component, OnInit, OnDestroy } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../../core/services/auth.service';
import { Store } from '@ngrx/store';
import { AppState } from '../../../interfaces';
import { Router, ActivatedRoute } from '@angular/router';
import { getAuthStatus, fetchGroups } from '../../reducers/selectors';
import { Subscription } from 'rxjs/Subscription';
import { normFieldConfig, ToasterModule, ToasterService, ToasterConfig, Toast } from '../../../shared/modules';
import { AuthActions } from '../../actions/auth.actions';
declare var $: any;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit, OnDestroy {
  title = environment.AppName;
  loginSubs: Subscription;
  returnUrl: string;
  signInForm: FormGroup;
  private groups = [];
  private toasterService: ToasterService;
  private authActions: AuthActions;
  public config1: ToasterConfig = new ToasterConfig({
    positionClass: 'toast-bottom-right'
  });
  constructor(
    private fb: FormBuilder,
    private store: Store<AppState>,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    toasterService: ToasterService,
    authActions: AuthActions
  ) {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    this.redirectIfUserLoggedIn();
    this.toasterService = toasterService;
    this.authActions = authActions;
    this.store.dispatch(this.authActions.getGroup());
  }

  popToast(type: string, title: string, body: string) {
    var toast: Toast = {
      type: type,
      title: title,
      body: body
    };

    this.toasterService.pop(toast);
  }

  ngOnInit() {
    this.initForm();
    this.authService.getGroups().subscribe((res)=>{
      let groups = res.json().items.list;
      for(let i = 0; i < groups.length; i++ ){
        let groupDetail = groups[i];
        this.groups.push({ label: groupDetail.groupName, value: groupDetail.groupName});
      }
    })
  }

  onSubmit(e) {
    const values = this.signInForm.value;
    const keys = Object.keys(values);
    console.log(this.user)
    if (this.signInForm.valid) {
      this.loginSubs = this.authService.login(this.user).subscribe(data => {
      
      });
    } else {
      keys.forEach(val => {
        const ctrl = this.signInForm.controls[val];
        if (!ctrl.valid) {
          this.pushErrorFor(val, null);
          ctrl.markAsTouched();
        };
      });
    }
  }

  private pushErrorFor(ctrl_name: string, msg: string) {
    this.signInForm.controls[ctrl_name].setErrors({ 'msg': msg });
  }

  initForm() {

    this.signInForm = this.fb.group({
    });
  }

  redirectIfUserLoggedIn() {
    this.store.select(getAuthStatus).subscribe(
      data => {
        if (data === true) { this.router.navigate([this.returnUrl]); }
      });
  }

  ngOnDestroy() {
    if (this.loginSubs) { this.loginSubs.unsubscribe(); }
  }

  userFields: normFieldConfig = [
    {
    className: '',
    fieldGroup: [
      // {
      //   type: 'radio',
      //   key: 'radio',
      //   templateOptions: {
      //     options: [{
      //       key: 'lol',
      //       value: 'lol',
      //     }, {
      //       key: 'sad',
      //       value: 'sad',
      //     }],
      //     label: 'Title',
      //     description: 'Please select',
      //   },
      // },
      // {
      //   key: 'textarea',
      //   type: 'textarea',
      //   modelOptions: {
      //     debounce: {
      //       default: 2000,
      //       blur: 0,
      //     },
      //     updateOn: 'default blur',
      //   },
      //   templateOptions: {
      //     rows: 5,
      //     cols: 20,
      //     label: 'Label',
      //     description: 'Please enter atleast 150.',
      //     focus: true,
      //   },
      // }, 
      // {
      //   key: 'toggle',
      //   type: 'toggle',
      //   templateOptions: {
      //     isAlert: true,
      //     isLarge: false,
      //   },
      // },
       {
        className: '',
        key: 'iss',
        type: 'select',
        templateOptions: {
          options: this.groups,
          placeholder: 'Select Group'
        },
        validators: {
          validation: Validators.compose([Validators.required])
        }
      },
      {
        className: '',
        key: 'username',
        type: 'input',
        templateOptions: {
          type: 'text',
          label: '',
          placeholder: 'Enter email/username'
        },
        validators: {
          validation: Validators.compose([Validators.required])
        }
      }, {
        className: '',
        key: 'password',
        type: 'input',
        templateOptions: {
          type: 'password',
          label: '',
          placeholder: 'Password',
          pattern: ''
        },
        validators: {
          validation: Validators.compose([Validators.required])
        },
      },
     

    ]
  }
  ];
  user = {
    username: '',
    password: '',
    iss: ''
  }


  ngAfterViewInit() {
    $(function () {
      $('.page-center').matchHeight({
        target: $('html')
      });

      $(window).resize(function () {
        setTimeout(function () {
          $('.page-center').matchHeight({ remove: true });
          $('.page-center').matchHeight({
            target: $('html')
          });
        }, 100);
      });
    });

  }
}
