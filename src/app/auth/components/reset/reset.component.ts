// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../core/services/auth.service';
import { normFieldConfig, ToasterModule, ToasterService, ToasterConfig, Toast } from '../../../shared/modules';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ValidationService } from '../../../shared/services';
declare var $: any
/**
 * 
 * 
 * @export
 * @class ResetComponent
 * @implements {OnInit}
 */
@Component({
    selector: 'app-reset',
    templateUrl: './reset.component.html',
    styleUrls: ['./reset.component.less']
})
export class ResetComponent implements OnInit {
    private authService: AuthService;
    private toasterService: ToasterService;
    private router: Router;
    public code: string;
    private account: string;
    private route: ActivatedRoute;
    private fb: FormBuilder;
    passwordForm: FormGroup;
    verifyForm: FormGroup;
    userFields: Array<normFieldConfig> = [];
    desabledButton:Boolean = true;
    options;
    user: any = {};
    constructor(
        authService: AuthService,
        toasterService: ToasterService,
        router: Router,
        route: ActivatedRoute,
        fb: FormBuilder) {
        this.authService = authService;
        this.toasterService = toasterService;
        this.route = route;
        this.router = router
        this.fb = fb;

        router.events.subscribe((val) => {
            this.code = this.route.snapshot.queryParams['code'];
            this.account = this.route.snapshot.queryParams['account'] || '';
        });

        this.options = {
            formState: {
                readOnly: true,
                submitted: false,
            },
        };
        this.passwordForm = fb.group({
        });
        
        let userFields: Array<normFieldConfig> = [{
            className: '',
            fieldGroup: [{
                className: '',
                key: 'password',
                type: 'input',
                templateOptions: {
                    type: 'password',
                    label: 'Password',
                    placeholder: 'Password',
                    required: true,
                    keyup: (field, formControl: FormControl) => {
                        console.log(formControl.valid ? 'Valid' : 'Invalid');
                    },
                },
                validation: {
                    show: false,
                },
                validators: {
                    validation: Validators.compose([Validators.required, Validators.maxLength(10), Validators.minLength(2)]),
                },
            }, {
                className: '',
                key: 'confirmPassword',
                type: 'input',
                templateOptions: {
                    type: 'password',
                    label: 'Confirm Password',
                    placeholder: 'Confirm Password',
                    tabindex: 1,
                    required: true,
                    keyup: (field, formControl: FormControl) => {
                       this.desabledButton = !formControl.valid;
                        console.log(formControl.valid ? 'Valid' : 'Invalid');
                    },
                },
                expressionProperties: {
                    'templateOptions.disabled': '!model.password'
                },
                validators: {
                    validation: Validators.compose([Validators.required, ValidationService.confirmPassword(this.passwordForm, 'password')]),
                },
            }]
        }];
        setTimeout(() => this.userFields = userFields);
        this.user = {
            account: this.account,
            code: ''
        };

    }

    ngOnInit() {
        this.code = this.route.snapshot.queryParams['code'];
    }

    public config1: ToasterConfig = new ToasterConfig({
        positionClass: 'toast-bottom-right'
    });

    popToast(type: string, title: string, body: string) {
        var toast: Toast = {
            type: type,
            title: title,
            body: body
        };

        this.toasterService.pop(toast);
    }

    onResetPassword() {
        this.authService.getCodeReset(this.user.account).subscribe(data => {
            if (data.json().status == 200) {
                this.router.navigateByUrl(`auth/reset?code=verify`);
            }
        });
    }

    onVerifyCode() {
        this.authService.verifyPassword(this.user.code).subscribe(data => {
            if (data.ok) {
                this.router.navigateByUrl('auth/reset?code=password');
            }
        });
    }

    onRestPasswordWithPassword() {
        this.authService.resetPassword({ password: this.user.password }).subscribe(data => {
            if (data.json().status != 500) {
                this.router.navigateByUrl('/');
            }
        });
    }

    private pushErrorFor(ctrl_name: string, msg: string) {
        this.verifyForm.controls[ctrl_name].setErrors({ 'msg': msg });
    }

    ngAfterViewInit() {
        $(function () {
            $('.page-center').matchHeight({
                target: $('html')
            });

            $(window).resize(function () {
                setTimeout(function () {
                    $('.page-center').matchHeight({ remove: true });
                    $('.page-center').matchHeight({
                        target: $('html')
                    });
                }, 100);
            });
        });
    }
}