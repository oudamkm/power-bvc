// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { Action } from '@ngrx/store';

export class AuthActions {
  static LOGIN = 'LOGIN';
  static LOGIN_SUCCESS = 'LOGIN_SUCCESS';
  static LOGOUT = 'LOGOUT';
  static LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
  static RESET = 'RESET';
  static RESET_SUCCESS = 'RESET_SUCCESS';
  static AUTHORIZE = 'AUTHORIZE';
  static GET_GROUP_SUCCESS = "GET_GROUP_SUCCESS";
  static GET_GROUP = "GET_GROUP";

  authorize(): Action {
    return { type: AuthActions.AUTHORIZE };
  }
   
  login(): Action {
    return { type: AuthActions.LOGIN };
  }

  loginSuccess(): Action {
    return { type: AuthActions.LOGIN_SUCCESS};
  }

  logout(): Action {
    return { type: AuthActions.LOGOUT };
  }

  logoutSuccess(): Action {
    return { type: AuthActions.LOGOUT_SUCCESS };
  }

  reset(): Action {
    return { type: AuthActions.RESET };
  }

  resetSuccess(): Action {
    return { type: AuthActions.RESET_SUCCESS };
  }

  getGroupSuccess(groups: any): Action {
    return { type: AuthActions.GET_GROUP_SUCCESS, payload: groups };
  }

  getGroup(): Action {
    return { type: AuthActions.GET_GROUP };
  }

}
