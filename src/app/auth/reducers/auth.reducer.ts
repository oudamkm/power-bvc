// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { Action, ActionReducer } from '@ngrx/store';
import { AuthActions } from '../actions/auth.actions';
import { AuthState, AuthStateRecord } from './auth.state';

export const initialState: AuthState = new AuthStateRecord() as AuthState;

export const authReducer: ActionReducer<AuthState> =
  (state: AuthState = initialState, { type, payload }: Action): AuthState => {
    switch (type) {
      case AuthActions.LOGIN_SUCCESS:
        return state.merge({ isAuthenticated: true }) as AuthState;

      case AuthActions.LOGOUT_SUCCESS:
        return state.merge({ isAuthenticated: false }) as AuthState;
    
      case AuthActions.RESET_SUCCESS:
        return state.merge({ isSended: true }) as AuthState;
        
      case AuthActions.GET_GROUP_SUCCESS:
        return state.merge({ 
          groups: payload.groups
        }) as AuthState;

      default:
        return state;
    }
  };
