// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { Map, Record } from 'immutable';

export interface AuthState extends Map<string, any> {
  isAuthenticated: boolean;
  isSended: boolean;
  groups : Map<string, any>;
}

export const AuthStateRecord = Record({
  isAuthenticated: false,
  isSended: false,
  groups: Map({})
});

