// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

export { Translator } from './Translator';
export { TranslateComponent } from './TranslateComponent';
export { TranslatorContainer } from './TranslatorContainer';
export { TranslatorModule, provideTranslator } from './TranslatorModule';