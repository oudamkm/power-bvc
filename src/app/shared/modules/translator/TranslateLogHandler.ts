// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

export class TranslateLogHandler {
    public error(message: string | Error): void {
        if (console && console.error) {
            console.error(message);
        }
    }

    public info(message: string): void {
    }

    public debug(message: string): void {
    }
}
