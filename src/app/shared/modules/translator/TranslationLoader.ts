// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

export abstract class TranslationLoader {
    public abstract load(options: any): Promise<object>;
}
