import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { normModule } from '../core/core';
import { BOOTSTRAP_norm_CONFIG, FIELD_TYPE_COMPONENTS } from './ui-bootstrap.config';
import { normValidationMessage } from './norm.validation-message';

@NgModule({
  declarations: [...FIELD_TYPE_COMPONENTS, normValidationMessage],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    normModule.forRoot(BOOTSTRAP_norm_CONFIG),
  ],
})
export class normBootstrapModule {
}
