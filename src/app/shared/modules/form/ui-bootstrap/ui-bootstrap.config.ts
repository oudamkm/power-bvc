import { ConfigOption } from '../core/services/norm.config';
import { normWrapperAddons } from './wrappers/addons';
import { TemplateDescription } from './run/description';
import { TemplateValidation } from './run/validation';
import { TemplateAddons } from './run/addon';
import {
  normFieldInput,
  normFieldCheckbox,
  normFieldRadio,
  normFieldSelect,
  normFieldTextArea,
  normFieldMultiCheckbox,
} from './types/types';
import {
  normWrapperLabel,
  normWrapperDescription,
  normWrapperValidationMessages,
  normWrapperFieldset,
} from './wrappers/wrappers';

export const FIELD_TYPE_COMPONENTS = [
  // types
  normFieldInput,
  normFieldCheckbox,
  normFieldRadio,
  normFieldSelect,
  normFieldTextArea,
  normFieldMultiCheckbox,

  // wrappers
  normWrapperLabel,
  normWrapperDescription,
  normWrapperValidationMessages,
  normWrapperFieldset,
  normWrapperAddons,
];

export const BOOTSTRAP_norm_CONFIG: ConfigOption = {
  types: [
    {
      name: 'input',
      component: normFieldInput,
      wrappers: ['fieldset', 'label'],
    },
    {
      name: 'checkbox',
      component: normFieldCheckbox,
      wrappers: ['fieldset'],
    },
    {
      name: 'radio',
      component: normFieldRadio,
      wrappers: ['fieldset', 'label'],
    },
    {
      name: 'select',
      component: normFieldSelect,
      wrappers: ['fieldset', 'label'],
    },
    {
      name: 'textarea',
      component: normFieldTextArea,
      wrappers: ['fieldset', 'label'],
    },
    {
      name: 'multicheckbox',
      component: normFieldMultiCheckbox,
      wrappers: ['fieldset', 'label'],
    },
  ],
  wrappers: [
    {name: 'label', component: normWrapperLabel},
    {name: 'description', component: normWrapperDescription},
    {name: 'validation-message', component: normWrapperValidationMessages},
    {name: 'fieldset', component: normWrapperFieldset},
    {name: 'addons', component: normWrapperAddons},
  ],
  manipulators: [
    {class: TemplateDescription, method: 'run'},
    {class: TemplateValidation, method: 'run'},
    {class: TemplateAddons, method: 'run'},
  ],
};
