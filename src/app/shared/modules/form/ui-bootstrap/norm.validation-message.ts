import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { normFieldConfig, normValidationMessages } from '../core/core';

@Component({
  selector: 'norm-validation-message',
  template: `{{errorMessage}}`,
})
export class normValidationMessage {
  @Input() fieldForm: FormControl;
  @Input() field: normFieldConfig;

  constructor(private normMessages: normValidationMessages) {}

  get errorMessage() {
    for (let error in this.fieldForm.errors) {
      if (this.fieldForm.errors.hasOwnProperty(error)) {
        let message = this.normMessages.getValidatorErrorMessage(error);

        if (this.field.validation && this.field.validation.messages && this.field.validation.messages[error]) {
          message = this.field.validation.messages[error];
        }

        ['validators', 'asyncValidators'].map(validators => {
          if (this.field[validators] && this.field[validators][error] && this.field[validators][error].message) {
            message = this.field.validators[error].message;
          }
        });

        if (typeof message === 'function') {
          return message(this.fieldForm.errors[error], this.field);
        }

        return message;
      }
    }
  }
}
