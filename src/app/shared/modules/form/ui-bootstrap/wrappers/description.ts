import { Component, ViewChild, ViewContainerRef } from '@angular/core';
import { FieldWrapper } from '../../core/core';

@Component({
  selector: 'norm-wrapper-description',
  template: `
    <ng-container #fieldComponent></ng-container>
    <div>
      <small class="text-muted">{{to.description}}</small>
    </div>
  `,
})
export class normWrapperDescription extends FieldWrapper {
  @ViewChild('fieldComponent', {read: ViewContainerRef}) fieldComponent: ViewContainerRef;
}
