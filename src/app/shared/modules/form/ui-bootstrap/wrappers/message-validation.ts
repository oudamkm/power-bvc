import { Component, ViewChild, ViewContainerRef } from '@angular/core';
import { FieldWrapper } from '../../core/core';

@Component({
  selector: 'norm-wrapper-validation-messages',
  template: `
    <ng-container #fieldComponent></ng-container>
    <div>
      <small class="text-muted text-danger" *ngIf="valid" role="alert" [id]="validationId"><norm-validation-message [fieldForm]="formControl" [field]="field"></norm-validation-message></small>
    </div>
  `,
})
export class normWrapperValidationMessages extends FieldWrapper {
  @ViewChild('fieldComponent', {read: ViewContainerRef}) fieldComponent: ViewContainerRef;

  get validationId() {
    return this.field.id + '-message';
  }
}
