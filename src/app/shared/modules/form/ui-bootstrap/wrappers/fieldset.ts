import { Component, ViewChild, ViewContainerRef } from '@angular/core';
import { FieldWrapper } from '../../core/core';

@Component({
  selector: 'norm-wrapper-fieldset',
  template: `
    <div class="form-group" [ngClass]="{'has-danger': valid}">
      <ng-container #fieldComponent></ng-container>
    </div>
  `,
})
export class normWrapperFieldset extends FieldWrapper {
  @ViewChild('fieldComponent', {read: ViewContainerRef}) fieldComponent: ViewContainerRef;
}
