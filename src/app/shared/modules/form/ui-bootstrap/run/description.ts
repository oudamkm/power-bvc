import { normFieldConfig, normConfig } from '../../core/core';

export class TemplateDescription {
  run(fc: normConfig) {
    fc.templateManipulators.postWrapper.push((field: normFieldConfig) => {
      if (field && field.templateOptions && field.templateOptions.description) {
        return 'description';
      }
    });
  }
}
