import { normFieldConfig, normConfig } from '../../core/core';

export class TemplateValidation {
  run(fc: normConfig) {
    fc.templateManipulators.postWrapper.push((field: normFieldConfig) => {
      if (field && field.validators) {
        return 'validation-message';
      }
    });
  }
}
