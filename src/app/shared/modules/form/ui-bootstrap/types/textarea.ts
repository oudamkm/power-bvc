import { Component } from '@angular/core';
import { FieldType } from '../../core/core';

@Component({
  selector: 'norm-field-textarea',
  template: `
    <textarea [name]="key" [formControl]="formControl" [cols]="to.cols"
      [rows]="to.rows" class="form-control"
      [normAttributes]="field">
    </textarea>
  `,
})
export class normFieldTextArea extends FieldType {
}
