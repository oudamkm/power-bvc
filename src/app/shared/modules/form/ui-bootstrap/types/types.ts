import { normFieldCheckbox } from './checkbox';
import { normFieldMultiCheckbox } from './multicheckbox';
import { normFieldInput } from './input';
import { normFieldRadio } from './radio';
import { normFieldTextArea } from './textarea';
import { normFieldSelect } from './select';

export {
  normFieldCheckbox,
  normFieldMultiCheckbox,
  normFieldInput,
  normFieldRadio,
  normFieldTextArea,
  normFieldSelect,
};
