import { Component } from '@angular/core';
import { FieldType } from '../../core/core';

@Component({
  selector: 'norm-field-input',
  template: `
    <input [type]="type" [formControl]="formControl" class="form-control"
      [normAttributes]="field" [ngClass]="{'form-control-danger': valid}">
    `,
})
export class normFieldInput extends FieldType {
  get type() {
    return this.to.type || 'text';
  }
}
