import { Component } from '@angular/core';
import { FormControl, AbstractControl } from '@angular/forms';
import { FieldType, normFieldConfig } from '../../core/core';

@Component({
  selector: 'norm-field-checkbox',
  template: `
    <label class="custom-control custom-checkbox">
      <input type="checkbox" [formControl]="formControl"
        *ngIf="!to.hidden" value="on"
        [normAttributes]="field" class="custom-control-input">
        {{to.label}}
        <span class="custom-control-indicator"></span>
    </label>
  `,
})
export class normFieldCheckbox extends FieldType {
  static createControl(model: any, field: normFieldConfig): AbstractControl {
    return new FormControl(
      { value: model ? 'on' : undefined, disabled: field.templateOptions.disabled },
      field.validators ? field.validators.validation : undefined,
      field.asyncValidators ? field.asyncValidators.validation : undefined,
    );
  }
}
