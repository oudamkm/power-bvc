import { Component } from '@angular/core';
import { FormGroup, FormControl, AbstractControl } from '@angular/forms';
import { FieldType, normFieldConfig } from '../../core/core';

@Component({
  selector: 'norm-field-multicheckbox',
  template: `
    <div *ngFor="let option of to.options" class="checkbox">
        <label class="custom-control custom-checkbox">
            <input type="checkbox" [value]="option.value" [formControl]="formControl.get(option.key)"
            [normAttributes]="field" class="custom-control-input">
            {{option.value}}
            <span class="custom-control-indicator"></span>
        </label>
    </div>
  `,
})
export class normFieldMultiCheckbox extends FieldType {
  static createControl(model: any, field: normFieldConfig): AbstractControl {
    let controlGroupConfig = field.templateOptions.options.reduce((previous, option) => {
      previous[option.key] = new FormControl(model ? model[option.key] : undefined);
      return previous;
    }, {});

    return new FormGroup(controlGroupConfig);
  }
}
