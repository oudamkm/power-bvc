import { FormGroup, AbstractControl } from '@angular/forms';
export interface normFieldConfig {
  key?: string;
  id?: string;
  name?: string;
  templateOptions?: normTemplateOptions;
  optionsTypes?: any;
  validation?: {
    messages?: {
      [messageProperties: string]: string | Function;
    };
    show?: boolean;
    [additionalProperties: string]: any;
  };
  validators?: any;
  asyncValidators?: any;
  template?: string;
  component?: any;
  wrapper?: string[] | string;
  wrappers?: string[];
  fieldGroup?: Array<normFieldConfig>;
  fieldArray?: normFieldConfig;
  hide?: boolean;
  formControl?: AbstractControl;
  hideExpression?: boolean | string | ((model: any, formState: any) => boolean);
  className?: string;
  type?: string;
  expressionProperties?: any;
  focus?: boolean;
  modelOptions?: any;
  lifecycle?: normLifeCycleOptions;
  defaultValue?: any;
  parsers?: [(value: any, index: number) => {}];
}

export interface normTemplateOptions {
  type?: string;
  label?: string;
  placeholder?: string;
  disabled?: Boolean;
  options?: Array<any>;
  rows?: number;
  cols?: number;
  description?: string;
  hidden?: boolean;
  max?: number;
  min?: number;
  minLength?: number;
  maxLength?: number;
  pattern?: string;
  required?: Boolean;
  tabindex?: number;
  step?: number;
  focus?: Function;
  blur?: Function;
  keyup?: Function;
  keydown?: Function;
  click?: Function;
  change?: Function;
  keypress?: Function;
  [additionalProperties: string]: any;
}

export interface normLifeCycleFn {
    (form?: FormGroup, field?: normFieldConfig, model?, options?): void;
}

export interface normLifeCycleOptions {
  onInit?: normLifeCycleFn;
  onChanges?: normLifeCycleFn;
  doCheck?: normLifeCycleFn;
  afterContentInit?: normLifeCycleFn;
  afterContentChecked?: normLifeCycleFn;
  afterViewInit?: normLifeCycleFn;
  afterViewChecked?: normLifeCycleFn;
  onDestroy?: normLifeCycleFn;
}
