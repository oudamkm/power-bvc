import { Component } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { FieldType } from '../templates/field.type';
import { clone } from '../utils';

@Component({
  selector: 'norm-group',
  template: `
    <norm-form [fields]="field.fieldGroup" [model]="model" [form]="normGroup" [options]="newOptions" [ngClass]="field.className" [buildForm]="false"></norm-form>
  `,
})
export class normGroup extends FieldType {

  get newOptions() {
    return clone(this.options);
  }

  get normGroup(): AbstractControl {
    if (this.field.formControl) {
      return this.field.formControl;
    } else {
      return this.form;
    }
  }
}
