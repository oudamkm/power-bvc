import { NgModule, ModuleWithProviders, ANALYZE_FOR_ENTRY_COMPONENTS } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { normForm } from './components/norm.form';
import { normFieldConfig } from './components/norm.field.config';
import { normField } from './components/norm.field';
import { normAttributes } from './components/norm.attributes';
import { normConfig, ConfigOption, norm_CONFIG_TOKEN } from './services/norm.config';
import { normFormBuilder } from './services/norm.form.builder';
import { normValidationMessages } from './services/norm.validation-messages';
import { normPubSub, normEventEmitter } from './services/norm.event.emitter';
import { Field } from './templates/field';
import { FieldType } from './templates/field.type';
import { FieldWrapper } from './templates/field.wrapper';
import { normGroup } from './components/norm.group';

export {
  ConfigOption,
  normAttributes,
  normFormBuilder,
  normField,
  normFieldConfig,
  normForm,
  normConfig,
  normPubSub,
  normValidationMessages,
  normEventEmitter,

  Field,
  FieldType,
  FieldWrapper,
};

const norm_DIRECTIVES = [normForm, normField, normAttributes, normGroup];

@NgModule({
  declarations: norm_DIRECTIVES,
  entryComponents: [normGroup],
  exports: norm_DIRECTIVES,
  imports: [
    CommonModule,
    ReactiveFormsModule,
  ],
})
export class normModule {
  static forRoot(config: ConfigOption = {}): ModuleWithProviders {
    return {
      ngModule: normModule,
      providers: [
        normFormBuilder,
        normConfig,
        normPubSub,
        normValidationMessages,
        { provide: norm_CONFIG_TOKEN, useValue: config, multi: true },
        { provide: ANALYZE_FOR_ENTRY_COMPONENTS, useValue: config, multi: true },
      ],
    };
  }

  static forChild(config: ConfigOption = {}): ModuleWithProviders {
    return {
      ngModule: normModule,
      providers: [
        { provide: norm_CONFIG_TOKEN, useValue: config, multi: true },
        { provide: ANALYZE_FOR_ENTRY_COMPONENTS, useValue: config, multi: true },
      ],
    };
  }
}
