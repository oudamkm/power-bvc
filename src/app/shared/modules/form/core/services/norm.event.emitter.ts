import { Subject } from 'rxjs/Subject';

export class normValueChangeEvent {
  constructor(public key: string, public value: any) {}
}

export class normEventEmitter extends Subject<String> {
  emit(value) {
    super.next(value);
  }
}

export class normPubSub {
  emitters = {};

  setEmitter(key, emitter) {
    this.emitters[key] = emitter;
  }

  getEmitter(key) {
    return this.emitters[key];
  }

  removeEmitter(key) {
    delete this.emitters[key];
  }
}
