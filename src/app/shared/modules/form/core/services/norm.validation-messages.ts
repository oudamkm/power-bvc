import { Inject, Injectable } from '@angular/core';
import { norm_CONFIG_TOKEN } from './norm.config';

@Injectable()
export class normValidationMessages {
  messages = {};

  constructor(@Inject(norm_CONFIG_TOKEN) configs = []) {
    configs.map(config => {
      if (config.validationMessages) {
        config.validationMessages.map(validation => this.addStringMessage(validation.name, validation.message));
      }
    });
  }

  addStringMessage(validator, message) {
    this.messages[validator] = message;
  }

  getMessages() {
    return this.messages;
  }

  getValidatorErrorMessage(prop) {
    return this.messages[prop];
  }
}
