// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { Component, OnInit } from '@angular/core';

@Component({
  selector: "nanita-button",
  template:`<fieldset class="form-group">
                  <button type="submit" class="btn">Login</button>
                </fieldset>`
})
export class ButtonComponent implements OnInit {
  Component(){

  }  
  ngOnInit(){

  }
}