// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { NgModule } from '@angular/core';
import { ButtonComponent } from './button.component';

@NgModule({
  imports:[],
  exports:[
    ButtonComponent
  ],
  declarations:[
    ButtonComponent
  ],
  providers:[]
})
export class ButtonModule {

}