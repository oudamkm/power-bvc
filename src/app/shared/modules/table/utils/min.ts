// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
  name: 'min'
})
export class MinPipe implements PipeTransform {
  transform(value: number[], args: string[]): any {
    return Math.min.apply(null, value);
  }
}
