// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

export const PAGINATION_STYLE = `
.pagination-box {
    position: relative;
    margin-top: -10px;
}
.pagination-range {
    margin-top: 7px;
    margin-left: 3px;
    display: inline-block;
}
.pagination-controllers {
    float: right;
}
.pagination-controllers input {
    min-width: 60px;
}

.pagination-limit {
    margin-right: 25px;
    display: inline-table;
    width: 150px;
}
.pagination-pages {
    display: inline-block;
}
.pagination-page {
    width: 110px;
    display: inline-table;
}
.pagination-box button {
    outline: none !important;
}
.pagination-prevpage,
.pagination-nextpage,
.pagination-firstpage,
.pagination-lastpage {
    vertical-align: top;
}
.pagination-reload {
    color: gray;
    font-size: 12px;
}
`;