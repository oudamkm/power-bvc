// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { Component, Inject, forwardRef, Output, EventEmitter} from '@angular/core';
import { DataTable } from './table.component';
import { PAGINATION_TEMPLATE } from './pagination.template';
import { PAGINATION_STYLE } from "./pagination.style";


@Component({
  selector: 'data-table-pagination',
  template: PAGINATION_TEMPLATE,
  styles: [PAGINATION_STYLE]
})
export class DataTablePagination {

    constructor(@Inject(forwardRef(() => DataTable)) public dataTable: DataTable) {}

    @Output() pageBackSelf = new EventEmitter();
    pageBack() {
        this.dataTable.offset -= Math.min(this.dataTable.limit, this.dataTable.offset);
        this.pageBackSelf.emit({pageSize: this.dataTable.limit, nowPage: this.dataTable.page});
    }


    @Output() pageForwardSelf = new EventEmitter();
    pageForwardSelfHandler($event){
        this.pageForwardSelf.emit($event);
    }
    
    pageForward($event) {
        this.dataTable.offset += this.dataTable.limit;
        this.pageForwardSelf.emit({pageSize: this.dataTable.limit, nowPage: this.dataTable.page});
    }

    @Output() pageFirstSelf = new EventEmitter();
    pageFirst() {
        this.dataTable.offset = 0;
        this.pageFirstSelf.emit({pageSize: this.dataTable.limit, nowPage: 1});
    }

    @Output() pageLastSelf = new EventEmitter();
    pageLast() {
        this.dataTable.offset = (this.maxPage - 1) * this.dataTable.limit;
        this.pageLastSelf.emit({pageSize: this.dataTable.limit, nowPage: this.maxPage});
    }

    get maxPage() {
        return Math.ceil(this.dataTable.itemCount / this.dataTable.limit);
    }

    get limit() {
        return this.dataTable.limit;
    }

    @Output() limitPage = new EventEmitter();
    set limit(value) {
        this.dataTable.limit = Number(<any>value); 
        this.limitPage.emit({pageSize: this.dataTable.limit});
    }

    get page() {
        return this.dataTable.page;
    }

    set page(value) {
        this.dataTable.page = Number(<any>value);
    }
}
