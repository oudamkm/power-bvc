// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { Component, Inject, forwardRef, Input, ViewChild } from '@angular/core';
import { DataTable } from './table.component';
import { HEADER_TEMPLATE } from './header.template';
import { HEADER_STYLE } from "./header.style";
import { provideTranslator } from '../../translator';
import { ModalComponent } from '../../modal/modal';
declare var $:any;
declare var jQuery:any;
interface JQuery {
    alert(options?:any):JQuery;
}  
@Component({
  selector: 'data-table-header',
  template: HEADER_TEMPLATE,
  styles: [HEADER_STYLE],
  host: {
    '(document:click)': '_closeSelector()'
  }
})
export class DataTableHeader {
    @ViewChild('modal')
    public modal: ModalComponent;
    columnSelectorOpen = false;
    @Input() isCheckedRow;
    _closeSelector() {
        this.columnSelectorOpen = false;
    }

    onDelete(e){
      this.modal.open();
    }
    constructor(@Inject(forwardRef(() => DataTable)) public dataTable: DataTable) {}
}
