// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

 export const HEADER_STYLE =
 `
.data-table-header {
    min-height: 25px;
    margin-bottom: 10px;
}
.title {
    display: inline-block;
    margin: 5px 0 0 5px;
}
.button-panel {
    float: right;
}
.button-panel button {
    outline: none !important;
}

.column-selector-wrapper {
    position: relative;
}
.column-selector-box {
    box-shadow: 0 0 10px lightgray;
    width: 150px;
    padding: 10px;
    position: absolute;
    right: 0;
    top: 1px;
    z-index: 1060;
    background-color:#fff;
}

.column-selector-box .checkbox {
    margin-bottom: 4px;
    
}
.checkbox input, .checkbox-bird input, .checkbox-detailed input, .checkbox-slide input, .checkbox-toggle input, .radio input{
    visibility: visible!important;
}
.column-selector-fixed-column {
    font-style: italic;
}

label > span {
    margin-left:20px;
}
.column-selector-button > .glyphicon{
    line-height: 0;
    top: 4px;
    font-size: 17px;
}
.columns.columns-right.btn-group.pull-right{
    width: 135px; 
}
.alert{
    display: none;
}
`;