// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

export const HEADER_TEMPLATE = `
   <div class="bars pull-left">
   <div id="toolbar">
        <div class="bootstrap-table-header" [textContent]="dataTable.headerTitle"></div>
          <button *ngIf="dataTable.showAdd" (click)="dataTable.addItems()" id="remove" class="btn btn-default remove">
            <i class="font-icon font-icon-close-2"></i> Add
          </button>
          <button *ngIf="dataTable.showUpdate" (click)="dataTable.updateItems()" id="remove" class="btn btn-success remove" [disabled]="!isCheckedRow">
            <i class="font-icon font-icon-close-2"></i> Update
          </button>
          <button *ngIf="dataTable.showDelete" (click)="onDelete()" id="remove" class="btn btn-danger remove" [disabled]="!isCheckedRow">
            <i class="font-icon font-icon-close-2"></i> Delete
          </button>
        </div>
   </div>
   <div class="columns columns-right btn-group pull-right">
    <!--button class="btn btn-default" type="button" name="paginationSwitch" title="Hide/Show pagination">
      <i class="font-icon font-icon-arrow-square-down"></i>
    </button-->

    <button (click)="dataTable.reloadItems()" class="btn btn-default" type="button" name="refresh" title="Refresh"><i class="font-icon font-icon-refresh"></i>
    </button>

    <button type="button" class="btn btn-default btn-sm column-selector-button" [class.active]="columnSelectorOpen"
            (click)="columnSelectorOpen = !columnSelectorOpen; $event.stopPropagation()" >
            <span class="glyphicon glyphicon-list"></span>
    </button>

    <button class="btn btn-default" type="button" name="toggle" title="Toggle"><i class="font-icon font-icon-list-square"></i>
    </button>

    <div class="column-selector-wrapper" (click)="$event.stopPropagation()">
            <div *ngIf="columnSelectorOpen" class="column-selector-box panel panel-default">
                <div *ngIf="dataTable.expandableRows" class="column-selector-fixed-column checkbox">
                    <label>
                        <input type="checkbox" [(ngModel)]="dataTable.expandColumnVisible"/>
                        <span>{{dataTable.translations.expandColumn | translate}}</span>
                    </label>
                </div>
                <div *ngIf="dataTable.indexColumn" class="column-selector-fixed-column checkbox">
                    <label>
                        <input type="checkbox" data-md-icheck  [(ngModel)]="dataTable.indexColumnVisible"/>
                        <span>{{dataTable.translations.indexColumn | translate}}</span>
                    </label>
                </div>
                <div *ngIf="dataTable.selectColumn" class="column-selector-fixed-column checkbox">
                    <label>
                        <input type="checkbox" [(ngModel)]="dataTable.selectColumnVisible"/>
                        <span>{{dataTable.translations.selectColumn | translate}}</span>
                    </label>
                </div>
                <div *ngFor="let column of dataTable.columns" class="column-selector-column checkbox">
                    <label>
                        <input type="checkbox" [(ngModel)]="column.visible"/>
                        <span [textContent]="column.header"></span>
                    </label>
                </div>
            </div>
        </div>
    <div class="export btn-group">
        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button"><i class="font-icon font-icon-download"></i> <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu">
            <li data-type="json"><a href="javascript:void(0)">JSON</a></li>
            <li data-type="xml"><a href="javascript:void(0)">XML</a></li>
            <li data-type="csv"><a href="javascript:void(0)">CSV</a></li>
            <li data-type="txt"><a href="javascript:void(0)">TXT</a></li>
            <li data-type="sql"><a href="javascript:void(0)">SQL</a></li>
            <li data-type="excel"><a href="javascript:void(0)">MS-Excel</a></li>
        </ul>
    </div>
</div>
<div class="pull-right search"><input class="form-control" type="text" placeholder="Search"></div>
<modal #modal>
    <modal-header [show-close]="true">
        <h4 class="modal-title" translate="header_add_job_title"></h4>
    </modal-header>
    <modal-body>
        <h1>Hweasdas</h1>
    </modal-body>
    <modal-footer>
         <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
         <a (click)="dataTable.deleteItems(modal,$event)" class="btn btn-danger btn-ok">Delete</a>
    </modal-footer>
   
</modal>
`;