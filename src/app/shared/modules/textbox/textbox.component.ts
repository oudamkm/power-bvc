// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { Component, Inject, OnInit, Input } from '@angular/core';

@Component({
	selector:"nanita-textbox",
	template:`<fieldset class="form-group">
									<label class="form-label">{{ label }}</label>
									<input 
											[value]="inputValue" 
											name="signin_v1[username]"
											[type]="type"
											placeholder="placeholder"
											class="form-control"
											data-validation="[NOTEMPTY]">
								</fieldset>`
})
export class TextBoxComponent implements OnInit{
	
	private inputValue: string;
	private type: string;
	private placeholder:string;
	@Input("label") label: string;
	constructor(){
			this.label = "This is test label";
			this.inputValue = '';
			this.type = "text";
			this.placeholder = "username";
	}
	
	ngOnInit(){

	}
}