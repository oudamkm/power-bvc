// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TextBoxComponent } from './textbox.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
    ],
    exports: [
        TextBoxComponent,
    ],
    declarations: [
        TextBoxComponent,
    ],
    providers: [],
})
export class TextBoxModule {

}