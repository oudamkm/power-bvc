// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

// Pipes
import { KeysPipe } from './pipes/keys.pipe';
import { HumanizePipe } from '../core/pipes/humanize.pipe';

// components
import { LoadingIndicatorComponent } from './components/loading-indicator/loading-indicator.component';
import { ToggleComponent } from './components/toggle.component';
// imports
import { DropdownModule } from 'ng2-bootstrap/dropdown';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NotificationComponent } from './components/notification/notification.component';
import { DataTableModule, TextBoxModule,
         ButtonModule, 
         normModule, normBootstrapModule,
         TranslatorModule,
         ToasterModule,
         provideTranslator,
         ModalModule
       } from './modules';

import {FormlyModule, FormlyBootstrapModule} from 'ng-formly';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// services
import { ValidationService } from './services/validation.service';
@NgModule({
  declarations: [
    // components
    LoadingIndicatorComponent,
    NotificationComponent,
    ToggleComponent,
    // pipes
    KeysPipe,
    HumanizePipe
  ],
  exports: [
    // components
    LoadingIndicatorComponent,
    NotificationComponent,
    ToggleComponent,
    // modules
    CommonModule,
    DropdownModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule,
    // pipes
    KeysPipe,
    HumanizePipe,
    normModule,
    DataTableModule,
    ToasterModule,
    TranslatorModule
  ],
  imports: [
    TextBoxModule,
    ButtonModule,
    DataTableModule,
    normModule.forRoot({
      types: [
        { name: 'toggle', component: ToggleComponent, defaultOptions: { templateOptions: { isAlert: false, isLarge: true }}}
      ]
    }),
    normBootstrapModule,
    TranslatorModule.forRoot({
      providedLanguages: ['km', 'en'],
      defaultLanguage: 'en',
      detectLanguage:  true
    }),
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    DropdownModule.forRoot(),
    ToasterModule,
    ModalModule
  ],
  providers: [
    ValidationService
  ]
})
export class SharedModule {}
