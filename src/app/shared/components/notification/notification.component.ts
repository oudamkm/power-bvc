// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { HttpService } from '../../../core/services/http';
import { provideTranslator } from '../../modules';
declare var $: any;

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.less'],
  providers: [provideTranslator('notify')]
})
export class NotificationComponent implements OnInit, OnDestroy {
  loading: any;
  notiSubs: Subscription;

  constructor(private httpInterceptor: HttpService) {
    this.notiSubs = this.httpInterceptor.loading.subscribe(
      data => {
        this.loading = data
        setTimeout(() => {
          $('#notie-alert-outer').hide();
        }, 5000)
      }
    );

  }

  onClose() {
    $('#notie-alert-outer').hide();
  }

  ngAfterViewInit() {

  }
  ngOnInit() {

  }

  ngOnDestroy() {
    this.notiSubs.unsubscribe();
  }

}
