// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { SearchActions } from './reducers/search.actions';
import { SharedModule } from './../shared/index';

// Components
import { HomeComponent } from './home.component';
// Breadcrumb components
import { BreadcrumbComponent } from './breadcrumb/components/breadcrumb/breadcrumb.component';

// Content components
import { CustomizeComponent } from './content/customize/customize.component';
import { ContentComponent } from './content/content'; 
// Sidebar components
import { TaxonsComponent } from './sidebar/taxons/taxons.component';
import { FilterComponent } from './sidebar/filter/filter.component';
import { MenusComponent } from './sidebar/menus/menus.component';

import { LayoutModule } from '../layout/index';
// Routes
import { HomeRoutes as routes } from './home.routes';

@NgModule({
  declarations: [
    // components
    HomeComponent,
    TaxonsComponent,
    FilterComponent,
    BreadcrumbComponent,
    CustomizeComponent,
    ContentComponent,
    MenusComponent
  ],
  exports: [
    MenusComponent
  ],
  imports: [
    RouterModule.forChild(routes),
    SharedModule,
    LayoutModule
  ],
  providers: [
    SearchActions
  ]
})
export class HomeModule {}
