// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { getSelectedTaxonIds } from './reducers/selectors';
import { environment } from './../../environments/environment';
import { AppState } from './../interfaces';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Component, OnInit, OnChanges } from '@angular/core';
import { AuthActions } from '../auth/actions/auth.actions';

@Component({
  selector: 'app-home',
  template: `
    <div class="page-content">
    <div class="container-fluid">
    <div class="row">
    </div>
    </div>
    </div>
  `,
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {
  
  selectedTaxonIds$: Observable<number[]>;

  constructor(private store: Store<AppState>,private authActions: AuthActions) {
    
  }

  ngOnInit() {
    
  }

}
