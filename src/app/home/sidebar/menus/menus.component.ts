// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { SearchActions } from './../../reducers/search.actions';
import { getFilters } from './../../reducers/selectors';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { AppState } from './../../../interfaces';
import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import * as $ from 'jquery';
import { provideTranslator } from '../../../shared/modules';

@Component({
    selector: 'app-menus',
    templateUrl: './menus.component.html',
    styleUrls: ['./menus.component.less'],
    providers: [ provideTranslator('menu') ]
})
export class MenusComponent implements OnInit {
    constructor() {
    }

    ngOnInit() {
    }

    ngAfterViewInit() {

        $('.side-menu-list li.with-sub').each(function () {
            var parent = $(this),
                clickLink = parent.find('>span'),
                subMenu = parent.find('>ul');

            clickLink.click(function () {
                if (parent.hasClass('opened')) {
                    parent.removeClass('opened');
                    subMenu.slideUp();
                    subMenu.find('.opened').removeClass('opened');
                } else {
                    if (clickLink.parents('.with-sub').length == 1) {
                        $('.side-menu-list .opened').removeClass('opened').find('ul').slideUp();
                    }
                    console.log(clickLink.parents('.with-sub'))
                    parent.addClass('opened');
                    subMenu.slideDown();
                }
            });
        });



    }
}