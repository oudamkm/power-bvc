// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { Router } from '@angular/router';
import { SearchActions } from './../../home/reducers/search.actions';
import { Component, OnInit, ChangeDetectionStrategy, Inject, Output, EventEmitter } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../interfaces';
import { getAuthStatus } from '../../auth/reducers/selectors';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../../core/services/auth.service';
import { AuthActions } from '../../auth/actions/auth.actions';
import { Translator, TranslatorContainer, provideTranslator } from '../../shared/modules';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.less'],
  providers: [ provideTranslator('header') ],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent implements OnInit {
  isAuthenticated: Observable<boolean>;
  @Output() changeLanguage = new EventEmitter();
  onLangaugeChange(e){
    this.changeLanguage.emit(e);
  }
   public avatarDataSquare: any;
  constructor(
    private store: Store<AppState>,
    private authService: AuthService,
    private authActions: AuthActions,
    private searchActions: SearchActions,
    private router: Router,
    @Inject(TranslatorContainer) public translatorContainer: TranslatorContainer 
  ) {
      this.translatorContainer = translatorContainer
      this.store.dispatch(this.authActions.authorize());
      this.isAuthenticated = this.store.select(getAuthStatus);
      this.avatarDataSquare = {
        size: 30,
        fontColor: '#FFFFFF',
        border: "1px solid #ddd",
        isSquare: false,
        text: JSON.parse(localStorage.getItem('me')).username
     };
  }

  ngOnInit() {
    
  }
  

  onLogout() {
    this.authService.logout().subscribe(
      data => console.log(data)
    );
  }
}
