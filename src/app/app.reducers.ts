// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { searchReducer } from './home/reducers/search.reducers';
import { environment } from './../environments/environment';
import { userReducer } from './user/reducers/user.reducer';
import { authReducer } from './auth/reducers/auth.reducer';
import { settingReducer } from './settings/reducers/settings.reducer';

import { combineReducers, ActionReducer } from '@ngrx/store';

import { AppState } from './interfaces';

import { compose } from '@ngrx/core/compose';

import { storeFreeze } from 'ngrx-store-freeze';

const reducers = {
  auth: authReducer,
  users: userReducer,
  search: searchReducer,
  settings: settingReducer
};

export const developmentReducer: ActionReducer<AppState> = compose(storeFreeze, combineReducers)(reducers); ;
const productionReducer: ActionReducer<AppState> = combineReducers(reducers);

/**
 * @export
 * @param {*} state
 * @param {*} action
 * @returns
 */
export function reducer(state: any, action: any) {
  if (environment.production) {
    return productionReducer(state, action);
  } else {
    return developmentReducer(state, action);
  }
}

