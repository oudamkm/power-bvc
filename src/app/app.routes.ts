// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { RouterModule, Routes } from '@angular/router';
import { CanActivateViaAuthGuard } from './core/guards/auth.guard';

export const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', 
    loadChildren: './home/index#HomeModule'
  },
  {
    path: 'user',
    loadChildren: './user/index#UserModule',
    canActivate: [ CanActivateViaAuthGuard ]
  },
  {
    path: 'settings',
    loadChildren: './settings/index#SettingModule',
    canActivate: [ CanActivateViaAuthGuard ]
  },
  { path: 'auth', loadChildren: './auth/auth.module#AuthModule' },
  { path: 'error', loadChildren: './error/index#ErrorModule'},
  { path: '**', redirectTo: 'error' }

];
