// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { Map, Record, List } from 'immutable';
import { User } from '../../core/models/user';

/**
 *
 *
 * @export
 * @interface UserState
 * @extends {Map<string, any>}
 */
export interface UserState extends Map<string, any> {
  user: User;
}

export const UserStateRecord = Record({
  user: Map({}),
  orders: List([])
});
