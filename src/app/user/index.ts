// Copyright 2017 Phat Sovathana. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

import { RouterModule } from '@angular/router';
import { UserRoutes } from './user.routes';
import { NgModule } from '@angular/core';

// components
import { UserComponent } from './user.component';
import { ProfileComponent } from './components/profile/profile.component'; 
// services
// import { UserService } from './services/user.service';

import { UserRoutes as routes } from './user.routes';
import { SharedModule } from '../shared/index';



@NgModule({
  declarations: [
    // components
    UserComponent,
    ProfileComponent
    // pipes

  ],
  exports: [

  ],
  providers: [
  ],
  imports: [
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class UserModule {}
